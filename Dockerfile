FROM node:lts-alpine3.16@sha256:418340baef60cf1c142e15d8378dcb0e7dd5145956c3a4ebbb0eba551c18a6f7

COPY package.json /opt/app/package.json

WORKDIR /opt/app

RUN apk add --no-cache python3 py3-pip python3-dev git curl jq yq git-lfs curl ca-certificates step-cli unzip && \
    pip install poetry && \
    yarn install --immutable --immutable-cache --check-cache

ENV PATH=$PATH:/root/node_modules/.bin:/opt/app/node_modules/.bin
