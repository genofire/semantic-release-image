## [0.0.31](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.30...v0.0.31) (2023-04-07)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9b1025f](https://gitlab.com/lmco/robots/semantic-release-image/commit/9b1025f8451ab094a92f52d28a5c28b36da10645))

## [0.0.30](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.29...v0.0.30) (2023-04-07)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5504f04](https://gitlab.com/lmco/robots/semantic-release-image/commit/5504f04456863b7b03f3bcb3a054a147d4ac96fa))

## [0.0.29](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.28...v0.0.29) (2023-04-01)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d5d32ca](https://gitlab.com/lmco/robots/semantic-release-image/commit/d5d32caa8839fa7b46fd9061efc39eaa6cb2c0dc))

## [0.0.28](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.27...v0.0.28) (2023-03-25)


### Bug Fixes

* **deps:** update dependency semantic-release to v21 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8693c5a](https://gitlab.com/lmco/robots/semantic-release-image/commit/8693c5aadeb2ae9395b2a2f34808bdf53cbc7778))

## [0.0.27](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.26...v0.0.27) (2023-03-23)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([72465de](https://gitlab.com/lmco/robots/semantic-release-image/commit/72465dea1b518b25d3f311815988f16a8e47ae08))

## [0.0.26](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.25...v0.0.26) (2023-03-23)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([12f2c1e](https://gitlab.com/lmco/robots/semantic-release-image/commit/12f2c1e236cf814b7c642f0fff85acf437afa805))

## [0.0.25](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.24...v0.0.25) (2023-03-22)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([18ba2f0](https://gitlab.com/lmco/robots/semantic-release-image/commit/18ba2f0ddabf6a818bc78151c5fd5a0ecb80ddb8))

## [0.0.24](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.23...v0.0.24) (2023-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e1f3a35](https://gitlab.com/lmco/robots/semantic-release-image/commit/e1f3a35d701de1faecc53595f8e49ed61addc948))

## [0.0.23](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.22...v0.0.23) (2023-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([040782c](https://gitlab.com/lmco/robots/semantic-release-image/commit/040782c2153ebff20a30d8d0f1f799c7bd658fbc))

## [0.0.22](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.21...v0.0.22) (2023-03-14)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ad6a7d3](https://gitlab.com/lmco/robots/semantic-release-image/commit/ad6a7d37f1634fa1d1b3913f90bdbd1f30d9af30))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([33c23b1](https://gitlab.com/lmco/robots/semantic-release-image/commit/33c23b1e8de0d778f3c0563af7a68b4c2fae3e47))

## [0.0.21](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.20...v0.0.21) (2023-03-13)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3edd5dd](https://gitlab.com/lmco/robots/semantic-release-image/commit/3edd5dd67d37a3f5e04176d150cd19a8875e91f0))

## [0.0.20](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.19...v0.0.20) (2023-03-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v9.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2c1fa3b](https://gitlab.com/lmco/robots/semantic-release-image/commit/2c1fa3b08cb8d9ae1930419cbcc9d799a8cdce7c))

## [0.0.19](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.18...v0.0.19) (2023-02-18)


### Bug Fixes

* Patch additional core dependencies ([d3dc6e9](https://gitlab.com/lmco/robots/semantic-release-image/commit/d3dc6e98bf9084d5981f14def96be8871c9c6a5a))

## [0.0.18](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.17...v0.0.18) (2023-01-11)


### Bug Fixes

* Add git-lfs ([43fe299](https://gitlab.com/lmco/robots/semantic-release-image/commit/43fe2996c18a38e1fc45be90e4a1acf8872639d8))

## [0.0.17](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.16...v0.0.17) (2022-12-08)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.5.1 ([254df88](https://gitlab.com/lmco/robots/semantic-release-image/commit/254df8830c80625068504928707bddea8e4556e1))

## [0.0.16](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.15...v0.0.16) (2022-11-29)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.2 ([25043e6](https://gitlab.com/lmco/robots/semantic-release-image/commit/25043e6b1d7c86f76d5bcf6f1119501ce3e27b7d))

## [0.0.15](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.14...v0.0.15) (2022-11-19)


### Bug Fixes

* Add python and poetry ([72185ab](https://gitlab.com/lmco/robots/semantic-release-image/commit/72185ab5e8a08dad4ad6aaa407cb2651024b2263))

## [0.0.14](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.13...v0.0.14) (2022-11-19)


### Bug Fixes

* Add semantic-release-python ([86ed2d5](https://gitlab.com/lmco/robots/semantic-release-image/commit/86ed2d52038c371eb2e370ee747f5e504d74671a))

## [0.0.13](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.12...v0.0.13) (2022-11-13)


### Bug Fixes

* Correct release artifact ([ae71c1e](https://gitlab.com/lmco/robots/semantic-release-image/commit/ae71c1e730e432a482673f77fa1f14346fedf303))

## [0.0.12](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.11...v0.0.12) (2022-11-13)


### Bug Fixes

* Cosign.pub name matters ([526c834](https://gitlab.com/lmco/robots/semantic-release-image/commit/526c834774adf0b69d9f027a167bffe20014722c))
* Remove gitlab.pub key ([e2b19fa](https://gitlab.com/lmco/robots/semantic-release-image/commit/e2b19faaed8f1fd611febd236ca122ef3ac63419))
* Use the digest in the bom name ([9417eb1](https://gitlab.com/lmco/robots/semantic-release-image/commit/9417eb193b34d047b36528defb562971ee739fa3))
* Verify signatures ([02016f7](https://gitlab.com/lmco/robots/semantic-release-image/commit/02016f70ca9ecb5a6a97b0268412121a8b5962ac))

## [0.0.11](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.10...v0.0.11) (2022-11-13)


### Bug Fixes

* Add build dependencies ([9385790](https://gitlab.com/lmco/robots/semantic-release-image/commit/9385790154fb65fe1ae9659f4b684e0509c3cd04))
* Change signing image ([502212e](https://gitlab.com/lmco/robots/semantic-release-image/commit/502212ee9d73ae71ba5808625e2e5dd85ac0e341))
* Change signing image ([500d151](https://gitlab.com/lmco/robots/semantic-release-image/commit/500d15198e1e93c23c2efe192505ab74e5eaa140))
* Cleanup apt install ([a1f5432](https://gitlab.com/lmco/robots/semantic-release-image/commit/a1f5432e4b520f1da260a602ce983867e9b3e9bc))
* Sign with hidden file ([a4fd8c6](https://gitlab.com/lmco/robots/semantic-release-image/commit/a4fd8c6db26f65a110d051f6809a6de1b5d06fd9))
* Sign with hidden file and bash ([c2fc376](https://gitlab.com/lmco/robots/semantic-release-image/commit/c2fc3766e577740af3dd6c8ed7cc9cf5a2c2445a))

## [0.0.10](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.9...v0.0.10) (2022-11-12)


### Bug Fixes

* Cleanup automation ([dd42dfd](https://gitlab.com/lmco/robots/semantic-release-image/commit/dd42dfdd7044e586318f0aa81dbe540a09b7b927))

## [0.0.9](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.8...v0.0.9) (2022-11-12)


### Bug Fixes

* Add dockerhub push ([861f2e6](https://gitlab.com/lmco/robots/semantic-release-image/commit/861f2e65ef82a44d15607c683f55a00703774088))
* Correct the docker endpoint ([e9d887b](https://gitlab.com/lmco/robots/semantic-release-image/commit/e9d887bfa3aa7d8add976b098601f50cfd245f43))
* Sign everything ([b87500c](https://gitlab.com/lmco/robots/semantic-release-image/commit/b87500cce656203d213241bd37c3a30ebe9ec2a1))
* Sign everything ([b640771](https://gitlab.com/lmco/robots/semantic-release-image/commit/b640771887f9daa05086bb6ec41831e92cad5c23))
* Sign everything ([dbd20ed](https://gitlab.com/lmco/robots/semantic-release-image/commit/dbd20ed659e823ca28fdbf2c6b70835bfbb5ce4f))
* Sign everything ([33f078c](https://gitlab.com/lmco/robots/semantic-release-image/commit/33f078ccbb6b3c93a56b20a601da8555b42dfbb1))

## [0.0.8](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.7...v0.0.8) (2022-11-11)


### Bug Fixes

* **deps:** update dependency semantic-release-helm to v2.2.0 ([a2929bc](https://gitlab.com/lmco/robots/semantic-release-image/commit/a2929bc3724bc7f7ba74e3413f176ce08bb44d21))

## [0.0.7](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.6...v0.0.7) (2022-11-11)


### Bug Fixes

* Correct readme ([3c0bb2f](https://gitlab.com/lmco/robots/semantic-release-image/commit/3c0bb2f9f33e06fcd5bb8a4af0f1f6462c23fac8))
* Correct semantic release command ([82bc239](https://gitlab.com/lmco/robots/semantic-release-image/commit/82bc239d00563865f02c4ce31f9105ac51d469c0))
* Identify artifact attestation ([6783bda](https://gitlab.com/lmco/robots/semantic-release-image/commit/6783bda109fc5da362d0d918bdf80511e6760ba7))
* Identify artifact attestation ([ba31256](https://gitlab.com/lmco/robots/semantic-release-image/commit/ba3125628456745ba3e2b708e7f7fcf25388cefc))
* Identify artifact attestation ([047d381](https://gitlab.com/lmco/robots/semantic-release-image/commit/047d3812c9b38f897c740b5d0809bfdc3615e19b))
* Identify artifact attestation ([eb1727b](https://gitlab.com/lmco/robots/semantic-release-image/commit/eb1727b6ee01f8a8e08415229e3574e246081aad))

## [0.0.6](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.5...v0.0.6) (2022-11-11)


### Bug Fixes

* Add readme ([da64a02](https://gitlab.com/lmco/robots/semantic-release-image/commit/da64a0237d7a3a77c7c4035e3c2670d58bce07f6))

## [0.0.5](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.4...v0.0.5) (2022-11-11)


### Bug Fixes

* Add readme ([6ae781c](https://gitlab.com/lmco/robots/semantic-release-image/commit/6ae781cabbacf9ae429888723b09dc17fb86ac3b))
* Correct git repository ([4858c4a](https://gitlab.com/lmco/robots/semantic-release-image/commit/4858c4a6962ea6ad71d72c6878da3c0af2f31b59))
* Correct git repository ([bf0740e](https://gitlab.com/lmco/robots/semantic-release-image/commit/bf0740e300e8e2a34060dc0e254a4117e14ecc34))
* Correct git repository ([949cf6a](https://gitlab.com/lmco/robots/semantic-release-image/commit/949cf6abd6be3c59e94d9a736082bf584448f6da))
* Correct git repository ([d322e77](https://gitlab.com/lmco/robots/semantic-release-image/commit/d322e7700039c86639d9b080c1114e3f3cbbf4c9))
* Verify signature ([c799bac](https://gitlab.com/lmco/robots/semantic-release-image/commit/c799bac94c41eea79e6b2669ef4027b514508774))

## [0.0.4](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.3...v0.0.4) (2022-11-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.5.0 ([7243bc1](https://gitlab.com/lmco/robots/semantic-release-image/commit/7243bc1eed1e7f82336a6297d792d9393a989799))

## [0.0.3](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.2...v0.0.3) (2022-11-11)


### Bug Fixes

* Use public rekor ([8cde505](https://gitlab.com/lmco/robots/semantic-release-image/commit/8cde5052efa6030a4a7d214cd2812a6baebe0340))

## [0.0.2](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.1...v0.0.2) (2022-11-11)


### Bug Fixes

* Reset versions and attach pub key ([dae1573](https://gitlab.com/lmco/robots/semantic-release-image/commit/dae15737f7a16095976e458aa6320529b9ccd84a))
